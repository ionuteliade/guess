<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WorkController@index')->name('home');
Route::post('/store_no', 'WorkController@store_no')->name('store_no');
Route::get('/guess_the_number', 'WorkController@guess_the_number')->name('guess_the_number');
Route::post('/store_guess', 'WorkController@store_guess')->name('store_guess');
Route::get('/play_again', 'WorkController@play_again')->name('play_again');
