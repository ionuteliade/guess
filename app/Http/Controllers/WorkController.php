<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Number;
use App\Guess;
use Illuminate\Support\Facades\DB;


class WorkController extends Controller
{
    public function index()
    {
        (session()->get('random') != NULL) ?: session()->put('random', Str::random(32));
        $ses = session()->get('random');
        return view ('index');
    }
    public function store_no()
    {
        if(Number::where('ses', session()->get('random'))->count() == 0)
        {
            $no =  rand(1, 50);
            $ses = session()->get('random');
            $row = new Number();
            $row->no = $no;
            $row->ses  = $ses;
            $row->save();
        }
        return redirect()->route('guess_the_number');
    }
    public function guess_the_number()
    {
        $ses =  session()->get('random');
        $no = DB::table("number")
        ->where('ses', $ses)
        ->get();
        $data['end'] = FALSE;
        $data['id_no'] = $id_no = $no[0]->id_no;
        $data['no'] = $no[0]->no;
        $data['nr_try'] = Guess::where('id_no', $no[0]->id_no)->count();

        $data['guess'] = $guess = DB::table('guess')
        ->where('id_no',  $id_no)
        ->get();
        foreach($guess as $row):
            ($row->no == $no[0]->no) ? $data['end'] = TRUE : $data['end'] = FALSE;
        endforeach;
        return view ('guess_the_number', ['data' => $data]);

    }
    public function store_guess(Request $request)
    {
        $request->validate([
            'no' => 'required|integer|between:1,50'
        ]);
        $row = new Guess();
        $row->no = $request->input('no');
        $row->id_no  = $request->input('id_no');
        $row->save();
        return redirect()->route('guess_the_number');
    }
    public function play_again()
    {
        session()->forget('random');
        return redirect()->route('home');
    }
}
