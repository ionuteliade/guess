<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guess extends Model
{
    protected $table = 'guess';
}
