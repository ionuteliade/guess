@extends('layout')
@section('content')
<div class="container-fluid">
    <div class="row p-2">
        <div class="col text-center">
            <h2>Guess the Number</h2>
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            You need to guess a number from 1 to 50<br>
            You have <b>{{ 6 - $data['nr_try'] }}</b> remaining attempts
            @if($errors->any())
            <div style="color:red;">
                @foreach($errors->all() as $error)
                    {{ $error }}<br>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            @foreach ($data['guess'] as $row)
            @if ($row->no == $data['no'])
            Congratulations, you've guessed the number: <b>{{$data['no']}}</b><br>
            @elseif($row->no < $data['no'])
            {{ $row->no }} is < that Number<br>
            @elseif($row->no > $data['no'])
            {{ $row->no }} is > that Number<br>
            @endif
            @endforeach
        </div>
    </div>

    @if(($data['nr_try'] < 6 ) && ($data['end'] == FALSE))
    <div class="row">
        <div class="col text-center p-3">
            <form action="{{ route('store_guess') }}" method="POST" enctype="multipart/form-data">
                @csrf
                No: <input type="text" style="width:50px; text-align:center;" name="no" required placeholder="1-50">
                <input type="hidden" name="id_no" value="{{$data['id_no']}}">
                <input type="submit"   value="Guess" class="btn btn-primary">
            </form>
        </div>
    </div>
    @else

    <div class="row">
        <div class="col text-center p-3">
            <a class="btn btn-info" href="play_again">Play again</a>
        </div>
    </div>
    @endif
</div>
@endsection
