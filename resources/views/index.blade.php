@extends('layout')
@section('content')
<div class="container-fluid">
    <div class="row p-2">
        <div class="col text-center">
            <h2>Guess the Number</h2>
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            You need to guess a number from 1 to 50
        </div>
    </div>
    <div class="row">
        <div class="col text-center p-3">
            <form action="{{ route('store_no') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="submit" value="Generate Number" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection
